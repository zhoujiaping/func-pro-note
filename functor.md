# 函子
```
class Functor f where
	fmap :: (a -> b) -> f a -> f b
```
函子是一个类型类。

实现了函子的类型（必定是一个一元参数化类型），必须实现fmap函数。

这个类型的实例，可以被看成一个值的容器。

fmap函数，将普通函数（kind为 `*->*`）转换成一个作用于容器的函数。

“容器”的概念，用于解释一元类型参数为普通类型时是ok的。

但是如果参数化类型是函数类型，我们就需要更抽象一点解释了。

函数类型也是函子。

首先要区分一下概念，函数类型和函数实例。

函数类型也是一个一元参数化类型，并且也能找到一个符合fmap定义的函数。
所以函数也是函子（当然还需要满足其他必要的定律，这里只是一种理解思路）。

对于函数，其fmap方法是函数的复合（why?因为compose是我们找到的满足fmap定义的函数）。
```
instance Functor ((->) r) where
	-- fmap f g = (\x -> f (g x))
	fmap = (.)
```
函子定义范畴间的映射。

# 可应用函数的函子
```
class Functor f => Applicative f where
	pure :: a -> f a
	(<*>) :: f (a -> b) -> f a -> f b
```
pure将一个普通值放进容器。

<*>将一个（包含函数的）容器，转换成一个参数和返回值都为容器的函数。

对于一个普通值，我们可以先用pure把它放进容器，然后把应用普通值的函数用<*>转换，再然后调用转换后的函数。这样我们就相当于没有拆开容器，而调用了容器里面函数。

可应用函数的函子，解决的问题是，如果函数被装进了容器中，我们也可以调用。

如果只是普通Functor，我们只能对没装进容器的函数转换，再调用。对容器中的函数束手无策。

并且pure提供了一种函数调用的方式将一个值/函数放进容器中。

否则，我们只能用值构造子来构造一个容器（类似于java的构造函数）。

函数也可以被定义为Applicative（也能找到符合pure和<*>定义的函数）。

# Monad
```
class Monad m where
	(>>=) :: m a -> (a -> m b) -> m b
	return :: a -> m a
```
Monad的(>>=)被称为bind函数。
java8的stream也是一种Monad，stream的flatmap就是这里的bind函数。
js中的Promise也是一种Monad，其then方法相当于flatmap函数。
```
promise.then(r1=>{
	...
	return new Promise(...){
		...
	}
}).then(r2=>{
	...
	return new Promise(...){
		...
	}
})
```

flatmap函数的作用：对于a -> m b这种态射，可以连续的进行调用。

如果没有Monad，我们就需要拆开容器，拿到里面的值，然后调用 a -> m b，然后再拆开容器，再拿到里面的值，然后调用 a -> m b...

return函数将一个值放进容器中。

Monad其实都可以定义为Applicative。（haskell中由于历史原因，没有这样定义）

严格意义上的Monad定义
```
class Applicative m => Monad m where
	return :: a -> m a
	join :: m (m a) -> m a
	(>>=) :: m a -> (a -> m b) -> m b
	(>>) :: m a -> m b -> mb

	return = pure
	join mma = mma >>= id
	(>>=) ma m = join $ fmap m ma 
	(>>) ma mb = ma $ >>= \_ -> mb
```
join就是将构造器脱去一层。

haskell programing上面说Moand意思为代码的整合、合并。

Monad总是代表着有一系列效应（effect）的运算。

阅读下面这篇文章，将会有好的体会。

https://blog.csdn.net/weixin_34314962/article/details/87948042

Monad解决函数式编程处理副作用时的两个问题。

1 每个函数都要被包裹成懒惰函数。

2 懒惰函数的连续调用。




