# 这一篇讲函数式编程处理副作用

本文基于 [译] 如何使用纯函数式 JavaScript 处理脏副作用 这篇文章
https://blog.csdn.net/weixin_34314962/article/details/87948042

三种方式
1 依赖注入
2 懒函数
3 返回值


## 依赖注入
```
function doSomething(){
    a()//a() it not a pure function
    b()
    c()
}
```
=>
```
function doSomething(a){
    a()
    b()
    c()
}
```
问题
1 全局变量很多，都转成函数参数，参数过多
2 参数钻井问题

思考：能否通过mock全局变量代替这种方式？
如果能把所有副作用的地方都mock掉，那是可以都。
这种方式还有其他问题，比如不看内部实现，我们不知道它是否为纯函数，也就是说函数上面没有pure/effect标记。

## 懒函数
```
function doSomething(){
    a()//a() it not a pure function
    b()
    c()
}
```
=>
```
function fa(){
    return ()=>a()
}
function fdoSomething(){
    return ()=>{
        fa()()
        b()
        c()
    }
}

fdoSomething()()
```
问题：
每个函数都要包装一个懒函数版本。
原来处理值的函数，要包装成处理懒函数的版本。
如果传入一个值和一个懒函数，怎么办？
懒函数的连续调用。
区分懒函数与普通函数。

## 返回值
```
function a(){
    println("hello")
}
function doSomething(){
    a()//a() it not a pure function
    b()
    c()
}
```
=>
```
function doSomething(){
    var effectPart = "hello"//a() it not a pure function
    b()
    var purePart = c()
    return [purePart,effectPart]
}

var res = doSomething()
var r1 = res[0]
println(res[1])//move side effect code outside
```
问题：
后面步骤依赖前面的返回值咋办？
事实上，这也是一种Monad，是一种Writer Monad，用来累加副作用。
也就是说函数中并不依赖副作用产生的任何结果，仅仅是产生了一些副作用，而不依赖副作用。

纯函数：
无副作用
引用透明（给定相同的入参，必定返回相同的结果）

没有发生的副作用，不算副作用。

针对懒函数版本的问题，提出优化方案，Effect。
每个值可以被包装成懒函数。
每个函数可以被包装成懒函数。
通过判断是否为Effect对象可以被区分一个对象是懒函数还是普通函数。
提供连续执行的方法。
再提供一个接受普通值/普通函数作为参数，以Effect为返回值的函数（Monad的flatMap），
用来解决连续的从纯函数世界到非纯函数的世界的调用问题。

类似于
```
promise.then(r1=>
    new Promise(function(resolve,reject){
        ...
    }
}).then(r2=>
    new Promise(function(resolve,reject){
        ...
    }
}).then(r3=>
    new Promise(function(resolve,reject){
    ...
    }
})
```

这里的Effect，其实就是一个monad。

Monad的作用

1 使用懒函数处理副作用的时候，我们要为纯函数包装一个懒函数版本，
Monad通过提供pure函数、map函数和flatmap函数，使我们不再需要这样做。

2 使用懒函数处理副作用的时候，如果我们的参数，一部分是值，一部分是懒函数，如何调用纯函数？
我们需要为每一个这样的场景包装一个懒函数版本。
Monad通过提供pure函数、map函数和flatmap函数，使我们不再需要这样做。

3 pure函数将普通值包装成懒函数。在我们需要懒函数，而实际上我们只有一个值的场景有用。

4 组合懒函数的调用。
即在不实际执行副作用的情况下，将多个懒函数调用串联起来。后一个懒函数依赖前一个懒函数的副作用。

5 通过类型标记一个函数是纯的还是有副作用的，从而将函数隔离成纯和不纯两个世界。
我们看到一个函数，根据类型判断其是纯函数后，就可以非常放心的去调用它，
或者用表达式/值去替换它，或者并行执行它。





